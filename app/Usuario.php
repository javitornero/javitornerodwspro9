<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios'; // Para decirle a LAravel que nuestra tabla se llama así
    protected $fillable = ['nom', 'id_loc'];
    public $timestamps=false; // Para evitar que nos cree los campos d creación y actualización que trae por defecto Laravel

    public function localizaciones() {
        return $this->belongsTo('App\Localizacion', 'id_loc');
    }
}
