<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localizacion extends Model
{
    protected $table = 'localizaciones'; // Para decirle a LAravel que nuestra tabla se llama así

    protected $fillable = ['nom'];

    public $timestamps=false; // Para evitar que nos cree los campos d creación y actualización que trae por defecto Laravel


    public function usuarios() {
        return $this->hasMany('App\Usuario', 'id');
    }
}
