<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Localizacion;

class LocalizacionController extends Controller
{
    public function index()
    {
        $localizaciones = Localizacion::all();
        return view('readLocalizaciones')->with('localizaciones',$localizaciones);
    }

    public function create()
    {
        $id = $this->lastId();
        return view('createLocalizacion')->with('id',$id);
    }

    public function store(Request $request)
    {   
        $localizacion = new Localizacion($request->all());
        $localizacion->save();
        flash('El localizacion '.$localizacion->nom.' ha sido creado con éxito', 'success');
        return redirect()->route('localizaciones');
    }

    public function show(Request $request)
    {
        $id = $request->id;
        if (Localizacion::find($id)) {
            $localizaciones = array();
            $localizacion = Localizacion::find($id);
            array_push($localizaciones, $localizacion);
            return view('readLocalizaciones')->with('localizaciones',$localizaciones);
        } else {
            flash('No existen coincidencias con el ID seleccionado', 'danger');
            return redirect()->route('localizaciones');
        }
    }

    public function edit($id)
    {
        $localizacion = Localizacion::find($id);
        return view('editLocalizacion')->with('localizacion',$localizacion);
    }

    public function update(Request $request, $id)
    {
        if (Localizacion::find($id)) {
            $localizacion  = Localizacion::find($id);
            $localizacion->nom = $request->nom;
            $localizacion->save();
            flash('La localizacion '.$localizacion->nom.' ha sido actualizado con éxito', 'success');
        } else {
            flash('La localizacion seleccionada no existe', 'danger');
        }
        return redirect()->route('localizaciones');
    }

    public function destroy($id)
    {
        $localizacion = Localizacion::find($id);
        $localizacion->delete();
        flash('La localizacion '.$localizacion->nom.' ha sido borrado con éxito', 'success');
        return redirect()->route('localizaciones');
    }
    
    public function lastId()
    {
        $id = DB::select('SELECT MAX(id) AS id FROM localizaciones');
        $id = $id[0]->id;
        $id++;
        return $id;
    }
}
