<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Localizacion;
use App\Usuario;

class UsuarioController extends Controller
{
    public function index()
    {
        $usuarios = Usuario::all();
        return view('readUsuarios')->with('usuarios',$usuarios);
    }

    public function create()
    {
        $id = $this->lastId();
        $localizaciones = Localizacion::all();
        return view('createUsuario')->with('id',$id)->with('localizaciones',$localizaciones);
    }

    public function store(Request $request)
    {   
        $usuario = new Usuario($request->all());
        $usuario->save();
        //flash('El usuario '.$usuario->nom.' ha sido creado con éxito', 'success');
        return redirect()->route('usuarios');
    }

    public function show(Request $request)
    {
        $id = $request->id;
        if (Usuario::find($id)) {
            $usuarios = array();
            $usuario = Usuario::find($id);
            array_push($usuarios, $usuario);
            return view('readUsuarios')->with('usuarios',$usuarios);
        } else {
            flash('No existen coincidencias con el ID seleccionado', 'danger');
            return redirect()->route('usuarios');
        }
    }

    public function edit($id)
    {
        $usuario = Usuario::find($id);
        $localizaciones = Localizacion::all();
        return view('editUsuario')->with('usuario',$usuario)->with('localizaciones',$localizaciones);
    }

    public function update(Request $request, $id)
    {
        if (Usuario::find($id)) {
            $usuario = Usuario::find($id);
            $usuario->nom = $request->nom;
            $usuario->id_loc = $request->localizacion;
            $usuario->save();
            flash('El usuario '.$request->nom.' ha sido actualizado con éxito', 'success');
        } else {
            flash('El usuario seleccionado no existe', 'danger');
        }
        return redirect()->route('usuarios');
    }

    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        $usuario->delete();
        flash('El usuario '.$usuario->nom.' ha sido borrado con éxito', 'success');
        return redirect()->route('usuarios');
    }
    
    public function lastId()
    {
        $id = DB::select("SELECT MAX(id) AS id FROM usuarios");
        $id = $id[0]->id;
        $id++;
        return $id;
    }
}
