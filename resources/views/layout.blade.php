<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans+Mono" rel="stylesheet">
    <title>@yield('titulo')</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css" /> -->
     <link href="{{{ asset('/css/estilo.css') }}}" rel="stylesheet">  

</head>
<body>
    <div class="divCabecera">
        <a class="aLogo" href="{{route('/')}}">
            <div class="divLogo">
                Gestión de usuarios
            </div>
        </a>
        <div class="triangulo"></div>
        <nav class="menu">
            <ul class="menu--barra-principal">
                <li class="menu--barra-principal--entrada">
                    <a href="#">Localizaciones</a>
                    <ul class="submenu">
                        <li id="createLoc" class="submenu--entrada"><a href="{{route('localizaciones.create')}}">Añadir</a></li>
                        <li id="readLoc" class="submenu--entrada"><a href="{{route('localizaciones')}}">Mostrar</a></li>
                    </ul>
                </li>
                <li class="menu--barra-principal--entrada">
                    <a href="#">Usuarios</a>
                    <ul class="submenu">
                        <li id="createU" class="submenu--entrada"><a href="{{route('usuarios.create')}}">Añadir</a></li>
                        <li id="readU" class="submenu--entrada"><a href="{{route('usuarios')}}">Mostrar</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <div id="contenedor">
        <div class="colOpciones">         
        </div>
        <div class="contenido">
            @yield('contenido')
        </div>
    </div>
    <footer>
        <div>
            <a class="aPie" href="docs/doc.pdf" target="_blank">Documentación</a>
        </div>
        <div>
        </div>
        <div>
            <a class="aPie" href="docs/DWST5PROYECTO5.pdf" target="_blank">Enunciado PROY.5</a><br>
            <a class="aPie" href="docs/DWST6PROYECTO6.pdf" target="_blank">Enunciado PROY.6</a>
        </div>
        <div>
            <a class="aPie" href="docs/DWST7PROYECTO7.pdf" target="_blank">Enunciado PROY.7</a><br>
            <a class="aPie" href="docs/DWST8PROYECTO8.pdf" target="_blank">Enunciado PROY.8</a>
        </div>
    </footer>
    
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script> -->
</body>
</html>
                        