@extends('layout')

@section('titulo', 'Crear usuario')

@section('contenido')
<div class="contenedorForm">
    <span class="cierraForm"><a class="aCierraForm" href="{{route('/')}}">&nbsp;x&nbsp;</a></span>
    <div class="divCRUD" id="divCreateU">
        <h1>Nuevo usuario</h1>
        <form method="POST" action="{{ route('usuarios.store') }}" >
            <input name="id" type="hidden" value="{{ $id }}"  readonly="readonly" />
            <table>
                <tr>
                    <td class="tdCRUD">Nombre nuevo usuario: </td>
                    <td class="tdCRUD"><input class="textCRUD" type="text" name="nom" required /></td>
                </tr>
                <tr>
                    <td class="tdCRUD">Localización del usuario: </td>
                    <td class="tdCRUD">
                        <select name="id_loc" required >
                        <option  selected disabled>Elige opción</option>
                        @foreach ($localizaciones as $localizacion)
                            <option value="{{ $localizacion->id }}">
                                {{ $localizacion->nom }}
                            </option>
                        @endforeach; 
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tdCRUD"><input type="submit" name="sbCreateU" value="Crear"></td>
                    <td class="tdCRUD"><input type="reset" name="Borrar"></td>
                </tr>
            </table>        
        </form>
    </div>
</div>    
@endsection