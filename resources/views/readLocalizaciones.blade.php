@extends('layout')

@section('titulo', 'Ver localizaciones')

@section('contenido')
    <div class="divReadRes" id="divReadLoc">
        <h1>Localizaciones</h1>
        <div class="panelScroll">
            <table class="table">
                <tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>Nombre</td><td class='colTabla'></td><td class='colTabla'></td></tr>
                
                @foreach ($localizaciones as $localizacion)
                <tr class='filaTabla'>
                    <td class='colTabla'>{{ $localizacion->id }}</td>
                    <td class='colTabla'>{{ $localizacion->nom }}</td>
                    <td class='colTabla'><a class='a--update' href={{route('localizaciones.edit', $localizacion->id)}}>Editar</a></td>
                    <td class='colTabla'><a class='a--delete' href={{route('localizaciones.destroy', $localizacion->id)}}>Eliminar</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection