@extends('layout')

@section('titulo', 'Ver usuarios')

@section('contenido')
    <div class="divReadRes" id="divReadU">
        <h1>usuarios</h1>
        <div class="panelScroll">
            <table class="table">
                <tr class='cabTabla'><td class='colTabla'>Id</td><td class='colTabla'>Nombre</td><td class='colTabla'>Localización</td><td class='colTabla'></td><td class='colTabla'></td></tr>
                
                @foreach ($usuarios as $usuario)
                <tr class='filaTabla'>
                    <td class='colTabla'>{{ $usuario->id }}</td>
                    <td class='colTabla'>{{ $usuario->nom }}</td>
                    <td class='colTabla'>{{ $usuario->localizaciones->nom }}</td>
                    <td class='colTabla'><a class='a--update' href={{route('usuarios.edit', $usuario->id)}}>Editar</a></td>
                    <td class='colTabla'><a class='a--delete' href={{route('usuarios.destroy', $usuario->id)}}>Eliminar</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection