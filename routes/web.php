<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => '/',
    'uses' => 'mainController@index'
]);

//Rutas de localizaciones
Route::get('localizaciones', [
    'as' => 'localizaciones',
    'uses' => 'LocalizacionController@index'
]);
Route::get('localizaciones/borrar/{id}', [
    'as' => 'localizaciones.destroy',
    'uses' => 'LocalizacionController@destroy'
]);
Route::get('localizaciones/editar/{id}', [
    'as' => 'localizaciones.edit',
    'uses' => 'LocalizacionController@edit'
]);
Route::get('localizaciones/crear', [
    'as' => 'localizaciones.create',
    'uses' => 'LocalizacionController@create'
]);
Route::get('localizaciones/guardar', [
    'as' => 'localizaciones.store',
    'uses' => 'LocalizacionController@store'
]);
Route::POST('localizaciones/actualizar/{id}', [
    'as' => 'localizaciones.update',
    'uses' => 'LocalizacionController@update'
]);
Route::get('localizaciones/buscar', [
    'as' => 'localizaciones.show',
    'uses' => 'LocalizacionController@show'
]);

//Rutas de usuarios
Route::get('usuarios', [
    'as' => 'usuarios',
    'uses' => 'UsuarioController@index'
]);
Route::get('usuarios/borrar/{id}', [
    'as' => 'usuarios.destroy',
    'uses' => 'UsuarioController@destroy'
]);
Route::get('usuarios/editar/{id}', [
    'as' => 'usuarios.edit',
    'uses' => 'UsuarioController@edit'
]);
Route::get('usuarios/crear', [
    'as' => 'usuarios.create',
    'uses' => 'UsuarioController@create'
]);
Route::get('usuarios/guardar', [
    'as' => 'usuarios.store',
    'uses' => 'UsuarioController@store'
]);
Route::POST('usuarios/actualizar/{id}', [
    'as' => 'usuarios.update',
    'uses' => 'UsuarioController@update'
]);
Route::get('usuarios/buscar', [
    'as' => 'usuarios.show',
    'uses' => 'UsuarioController@show'
]);